const {TwitterApi} = require('twitter-api-v2');
const fs = require('fs')


// main();

function initClient(){
    const credentialsPath = './credentials.json';
    const credentials = JSON.parse(fs.readFileSync(credentialsPath, 'utf8'));

    console.log(credentials)

    return new TwitterApi(credentials)
}

const client = initClient();

const rwClient = client.readWrite

module.exports = rwClient


/*

async function getUserDetails() {
  try {
    const user = await client.v2.userByUsername('your_twitter_username');
    console.log('User:', user.data);
  } catch (error) {
    console.error('Failed to fetch user details:', error);
  }
}

getUserDetails();



*/